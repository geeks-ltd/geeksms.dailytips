﻿// ********************************************************************
// WARNING: This file is auto-generated from M# Model.
// and may be overwritten at any time. Do not change it manually.
// ********************************************************************

namespace MSharp
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Olive.Entities;
    using Olive;
    using PeopleService;
    using Domain;
    using _F = System.Runtime.CompilerServices.CallerFilePathAttribute;
    using _L = System.Runtime.CompilerServices.CallerLineNumberAttribute;
    
    [EscapeGCop("Auto generated code.")]
    static partial class SchemaExtensions
    {
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> ExecutingInstance(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.ExecutingInstance, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> Heartbeat(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Heartbeat, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> IntervalInMinutes(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.IntervalInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> LastExecuted(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.LastExecuted, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> Name(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.BackgroundTask> TimeoutInMinutes(
            this ListModule<Domain.BackgroundTask>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.TimeoutInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Person> Email(
            this ListModule<PeopleService.Person>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Email, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Person> IsActive(
            this ListModule<PeopleService.Person>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.IsActive, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Person> Name(
            this ListModule<PeopleService.Person>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Person> Roles(
            this ListModule<PeopleService.Person>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Roles, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Role> FullName(
            this ListModule<PeopleService.Role>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.FullName, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<PeopleService.Role> Name(
            this ListModule<PeopleService.Role>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> DateAdded(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.DateAdded, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> Deliveries(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Deliveries, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> Roles(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Roles, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> RolesLinks(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.RolesLinks, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> Text(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Text, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticle> Title(
            this ListModule<Domain.WisdomArticle>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Title, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticleRolesLink> Role(
            this ListModule<Domain.WisdomArticleRolesLink>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Role, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomArticleRolesLink> Wisdomarticle(
            this ListModule<Domain.WisdomArticleRolesLink>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Wisdomarticle, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomDelivery> Article(
            this ListModule<Domain.WisdomDelivery>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.Article, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomDelivery> DateSent(
            this ListModule<Domain.WisdomDelivery>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.DateSent, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static PropertyFilterElement<Domain.WisdomDelivery> User(
            this ListModule<Domain.WisdomDelivery>.SearchComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Search(x => x.User, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> ExecutingInstance(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.ExecutingInstance, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> Heartbeat(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Heartbeat, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> IntervalInMinutes(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.IntervalInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> LastExecuted(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.LastExecuted, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> Name(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.BackgroundTask> TimeoutInMinutes(
            this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.TimeoutInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Person> Email(
            this ViewModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Email, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Person> IsActive(
            this ViewModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.IsActive, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Person> Name(
            this ViewModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Person> Roles(
            this ViewModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Roles, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Role> FullName(
            this ViewModule<PeopleService.Role>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.FullName, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<PeopleService.Role> Name(
            this ViewModule<PeopleService.Role>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> DateAdded(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.DateAdded, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> Deliveries(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Deliveries, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> Roles(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Roles, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> RolesLinks(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.RolesLinks, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> Text(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Text, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticle> Title(
            this ViewModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Title, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticleRolesLink> Role(
            this ViewModule<Domain.WisdomArticleRolesLink>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Role, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomArticleRolesLink> Wisdomarticle(
            this ViewModule<Domain.WisdomArticleRolesLink>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Wisdomarticle, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomDelivery> Article(
            this ViewModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Article, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomDelivery> DateSent(
            this ViewModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.DateSent, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static ViewElement<Domain.WisdomDelivery> User(
            this ViewModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.User, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, Guid?> ExecutingInstance(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.ExecutingInstance, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, DateTime?> Heartbeat(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Heartbeat, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, int> IntervalInMinutes(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.IntervalInMinutes, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, DateTime?> LastExecuted(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.LastExecuted, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, string> Name(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Name, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.BackgroundTask, int> TimeoutInMinutes(
            this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.TimeoutInMinutes, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Person, string> Email(
            this ListModule<PeopleService.Person>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Email, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Person, bool> IsActive(
            this ListModule<PeopleService.Person>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.IsActive, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Person, string> Name(
            this ListModule<PeopleService.Person>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Name, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Person, string> Roles(
            this ListModule<PeopleService.Person>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Roles, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Role, string> FullName(
            this ListModule<PeopleService.Role>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.FullName, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<PeopleService.Role, string> Name(
            this ListModule<PeopleService.Role>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Name, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, DateTime> DateAdded(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.DateAdded, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, IDatabaseQuery<Domain.WisdomDelivery>> Deliveries(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Deliveries, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, Task<IEnumerable<PeopleService.Role>>> Roles(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Roles, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, IDatabaseQuery<Domain.WisdomArticleRolesLink>> RolesLinks(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.RolesLinks, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, string> Text(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Text, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticle, string> Title(
            this ListModule<Domain.WisdomArticle>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Title, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticleRolesLink, PeopleService.Role> Role(
            this ListModule<Domain.WisdomArticleRolesLink>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Role, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomArticleRolesLink, Domain.WisdomArticle> Wisdomarticle(
            this ListModule<Domain.WisdomArticleRolesLink>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Wisdomarticle, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomDelivery, Domain.WisdomArticle> Article(
            this ListModule<Domain.WisdomDelivery>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.Article, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomDelivery, DateTime> DateSent(
            this ListModule<Domain.WisdomDelivery>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.DateSent, fl, ln);
        
        [MethodColor("#0CCC68")]
        public static ViewElement<Domain.WisdomDelivery, PeopleService.Person> User(
            this ListModule<Domain.WisdomDelivery>.ColumnComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Column(x => x.User, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement ExecutingInstance(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.ExecutingInstance, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement Heartbeat(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Heartbeat, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement IntervalInMinutes(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.IntervalInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement LastExecuted(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.LastExecuted, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement TimeoutInMinutes(
            this FormModule<Domain.BackgroundTask>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.TimeoutInMinutes, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Email(
            this FormModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Email, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static BooleanFormElement IsActive(
            this FormModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.IsActive, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(
            this FormModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Roles(
            this FormModule<PeopleService.Person>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Roles, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement FullName(
            this FormModule<PeopleService.Role>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.FullName, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(
            this FormModule<PeopleService.Role>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Name, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement DateAdded(
            this FormModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.DateAdded, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Deliveries(
            this FormModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Deliveries, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement RolesLinks(
            this FormModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.RolesLinks, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Text(
            this FormModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Text, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Title(
            this FormModule<Domain.WisdomArticle>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Title, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Role(
            this FormModule<Domain.WisdomArticleRolesLink>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Role, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Wisdomarticle(
            this FormModule<Domain.WisdomArticleRolesLink>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Wisdomarticle, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Article(
            this FormModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.Article, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement DateSent(
            this FormModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.DateSent, fl, ln);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement User(
            this FormModule<Domain.WisdomDelivery>.FieldComponents @this, [_F] string fl = null, [_L] int ln = 0)
        => @this.container.Field(x => x.User, fl, ln);
    }
}