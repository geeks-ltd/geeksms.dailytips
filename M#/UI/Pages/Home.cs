﻿using MSharp;

public class HomePage : RootPage
{
    public HomePage()
    {
        RouteRoot();
        Roles(AppRole.Employee, AppRole.Remote);
        Add<Modules.WisdomArticlesList>();
    }
}