namespace WisdomArticles
{
    public class EnterPage : MSharp.SubPage<HomePage>
    {
        public EnterPage()
        {
            Add<Modules.WisdomArticleForm>();
        }
    }
}