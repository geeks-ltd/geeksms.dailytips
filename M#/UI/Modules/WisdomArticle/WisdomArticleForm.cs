using MSharp;

namespace Modules
{
    public class WisdomArticleForm : MSharp.FormModule<Domain.WisdomArticle>
    {
        public WisdomArticleForm()
        {
            HeaderText("Daily tip Details");

            Field(x => x.Title);
            Field(x => x.RolesLinks).AsCollapsibleCheckBoxList();
            Field(x => x.Text).AsHtmlEditor().ExtraControlAttributes("data-toolbar=\"Compact\"");

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();
                x.ReturnToPreviousPage();
            });
        }
    }
}