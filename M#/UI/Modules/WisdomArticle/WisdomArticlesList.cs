using MSharp;

namespace Modules
{
    public class WisdomArticlesList : MSharp.ListModule<Domain.WisdomArticle>
    {
        public WisdomArticlesList()
        {
            HeaderText("Daily tips")
                .IndexColumn()
                .Sortable();

            // ================ Search: ================

            Search(GeneralSearch.AllFields).Label("Find:").AfterInput("[#BUTTONS(Search)#]");

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            LinkColumn("Title").HeaderText("Title")
                .Text(cs("item.Title"))
                .SortKey("Title")
                .OnClick(x =>
                {
                    x.Go<WisdomArticles.EnterPage>().SendReturnUrl().SendItemId();
                });

            Column(x => x.DateAdded);

            Column(x => x.Roles).SortingStatement("await item.Roles.ToString(\",\")")
                .SortKey("Roles");

            Column(x => x.Deliveries).DisplayExpression(cs("await item.Deliveries.Count()"))
                .SortingStatement("await item.Deliveries.Count()")
                .SortKey("DeliveriesCount");

            ImageColumn("DeleteCommand").HeaderText("Delete")
                .Text("Delete")
                .CssClass("btn btn-danger")
                .Icon(FA.Remove)
                .GridColumnCssClass("actions")
                .ConfirmQuestion("Are you sure you want to delete this Wisdom article?")
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.Reload();
                });

            ButtonColumn("Send to me").HeaderText("Test email")
                .Icon("fas fa-envelope")
                .CssClass("btn btn-info")
                .OnClick(x =>
                {
                    x.CSharp("item.SendAsEmail(await Context.Current.Person());");

                    x.GentleMessage(cs("\"Test email sent to \" + (await Context.Current.Person()).Email"));
                });

            Button("Add").Icon(FA.Plus)
                .OnClick(x => x.Go<WisdomArticles.EnterPage>().SendReturnUrl());
        }
    }
}