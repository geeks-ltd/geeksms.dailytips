using PeopleService;

namespace Domain
{
    public class WisdomDelivery : MSharp.EntityType
    {
        public WisdomDelivery()
        {
            DateTime("Date sent").Mandatory();

            Associate<Person>("User");

            Associate<WisdomArticle>("Article").Mandatory().DatabaseIndex();
        }
    }
}