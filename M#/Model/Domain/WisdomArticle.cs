using PeopleService;

namespace Domain
{
    public class WisdomArticle : MSharp.EntityType
    {
        public WisdomArticle()
        {
            String("Title").Mandatory();

            BigString("Text", 10).Lines(10);

            DefaultSort = DateTime("Date added").Mandatory().Default(cs("LocalTime.Now"));

            AssociateManyToMany<Role>("Roles");

            InverseAssociate<WisdomDelivery>("Deliveries", inverseOf: "Article");
        }
    }
}