﻿using Olive;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PeopleService
{
    public partial class Person
    {
        public static Task<Person[]> ActiveUsers()
        {
            return Context.Current.Database().GetList<Person>(x => x.IsActive);
        }

        internal async Task<bool> IsInAnyRoleAsync(Task<IEnumerable<Role>> roles)
        {
            foreach (var roleName in Roles.Split(","))
            {
                if (await roles.Any(x => x.Name.Contains(roleName, StringComparison.OrdinalIgnoreCase)))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
