﻿namespace Domain
{
    using EmailService;
    using Olive;
    using PeopleService;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the business logic for WisdomArticle class.
    /// </summary>
    partial class WisdomArticle
    {
        const int START_HOUR = 9, LATEST_HOUR = 16;

        public static async Task DeliverAsync()
        {
            if (LocalTime.Now.IsEnglishHoliday()) return;
            if (LocalTime.Now.Hour < START_HOUR || LocalTime.Now.Hour > LATEST_HOUR) return;

            foreach (var user in await Person.ActiveUsers())
                await DeliverNext(user);
        }

        static async Task DeliverNext(Person user)
        {
            var relevant = await Database.GetList<WisdomArticle>().WhereAsync(a => user.IsInAnyRoleAsync(a.Roles)).ToList();

            var toSend = await relevant.Except(a => a.IsSentToAsync(user)).FirstOrDefault();

            if (toSend == null) return;

            toSend.Send(user);
        }

        void Send(Person user)
        {
            Database.Save(new WisdomDelivery { Article = this, DateSent = LocalTime.Now, User = user });
            SendAsEmail(user);
        }

        Task<bool> IsSentToAsync(Person user)
        {
            return Database.Any<WisdomDelivery>(x => x.User == user && x.Article == this);
        }

        public void SendAsEmail(Person user)
        {
           CreateEmail(user).Publish();
        }

        private SendEmailCommand CreateEmail(Person user)
        {
            var image = Directory.GetCurrentDirectory().AsDirectory().GetSubDirectory("wwwroot").GetSubDirectory("images").GetSubDirectory("wise-man").GetFiles().PickRandom();
            return new SendEmailCommand
            {
                FromName = "Wise Man",
                To = user.Email,
                Html = true,
                Subject = Title,
                Body = $@"<h2>{Title}</h2>
                    <img src='https://training.geeksltd.co.uk/images/wise-man/{image.Name}' style='float:right; max-width:300px'/>                        
                <div style='float:left; max-width:570px'>{Text}</div>"
            };
        }

    }
}