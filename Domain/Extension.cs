﻿using Olive;
using PeopleService;
using System;
using System.Threading.Tasks;

namespace Domain
{
    public static class Extension
    {
        public static async Task<Person> Person(this Context @this)
        {
            var userId = @this.Http().User.GetId().TryParseAs<Guid>() ?? throw new Exception("No current user!");

            return await @this.Database().Get<Person>(userId);
        }
    }
}
