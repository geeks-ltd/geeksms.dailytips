﻿namespace Website
{
    using Microsoft.AspNetCore;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Olive;
    using Olive.Logging;

    public class Program
    {
        public static void Main(string[] args) => BuildWebHost(args).Run();

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging(ConfigureLogging)
                .UseStartup(typeof(Startup).Assembly.FullName).Build();
        }
        static void ConfigureLogging(WebHostBuilderContext context, ILoggingBuilder logging)
        {
            if (context.HostingEnvironment.IsProduction())
                logging.AddEventBus();
        }
    }
}