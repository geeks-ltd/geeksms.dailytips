﻿namespace Website
{
    using Microsoft.AspNetCore.Authentication.Cookies;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Olive;
    using Olive.Aws;
    using Olive.Entities.Data;
    using System;
    using System.Globalization;

    public abstract class Startup : FS.Shared.Website.Startup<Domain.ReferenceData, Domain.BackgroundTask, TaskManager>
    {
        public Startup(IWebHostEnvironment env, IConfiguration config, ILoggerFactory loggerFactory) : base(env, config, loggerFactory)
        {
        }

        protected override SecretProviderType GetSecretsProvider() => SecretProviderType.SystemsManagerParameter;

    }

   
}